FROM mcr.microsoft.com/dotnet/aspnet:5.0
COPY bin/Release/netcoreapp5.0/publish/ App/
WORKDIR /App
RUN dotnet new mvc --force
RUN dotnet build
RUN dotnet run
ENTRYPOINT ["dotnet", "MyWebApp.dll"]
